/* eslint-env phantomjs, browser */
/* globals axs returnTitle */

/**
 * Conducts audits using the Chrome Accessibility Tools and PhantomJS.
 */

/*
 * Options (opts)
 * - timeout         set the timeout interval - default to 9000
 * - filterRequests  set the network requests to filter out (ad block)
 * - retryCount      set the retry count for timed out network requests (will retry x times before failing)
 *
 */
var system             = require('system');
var webpage            = require('webpage').create();
var opts               = JSON.parse(system.args[1]);
var PAGE_TIMEOUT       = opts.timeout || 9000;
var RETRY_COUNT        = opts.retryCount || 3;
var TOOLS_PATH         = 'node_modules/accessibility-developer-tools/dist/js/axs_testing.js';
var BIND_POLYFILL_PATH = 'node_modules/phantomjs-polyfill/bind-polyfill.js';
var retry = {count: 0};

function formatTrace(trace) {
  var src = trace.file || trace.sourceURL;
  var fn  = (trace.function ? ' in function ' + trace.function : '');
  return '→ ' + src + ' on line ' + trace.line + fn;
}

// console.error is broken in PhantomJS
console.error = function () {
  system.stderr.writeLine([].slice.call(arguments).join(' '));
};

// Need to polyfill bind...
webpage.onInitialized = function () {
  webpage.injectJs(BIND_POLYFILL_PATH);
};

webpage.settings.resourceTimeout = PAGE_TIMEOUT;

webpage.viewportSize = {
  width: opts.width,
  height: opts.height
};

// filter resource requests (i.e. block ads and other non-vital resources)
webpage.onResourceRequested = function(requestData, networkRequest) {
  if (opts.hasOwnProperty('filterRequests')) {
    var regexp = new RegExp(opts.filterRequests, 'g');
    if (regexp.test(requestData.url)) {
      console.error(requestData.method.toUpperCase() + ' ' + requestData.url + ' net::ERR_BLOCKED_BY_CLIENT');
      networkRequest.abort();
    }
  }
};

function capture(retries) {
  window.setTimeout(function () {
    var ret = webpage.evaluate(function () {
      var results = axs.Audit.run();
      var pageTitle = document.title;

      // I just want to return the results, we don't need to do anything else here, the rest can be done from the task runner!!!

      var audit = results.map(function (result) {
        var DOMElements = result.elements;
        var message = '';


        if (DOMElements !== undefined) {
          for (var i = 0; i < DOMElements.length; i++) {
            var el = DOMElements[i];
            message += '\n';

              // Get query selector not browser independent. catch any errors and
              // default to simple tagName.
              try {
                message += axs.utils.getQuerySelectorText(el);
              } catch (err) {
                message += ' tagName:' + el.tagName;
                message += ' id:' + el.id;
              }
            }
          }

          return {
            code: result.rule.code,
            heading: result.rule.heading,
            result: result.result,
            severity: result.rule.severity,
            url: result.rule.url,
            elements: message
          };
        });

      return {
        title: pageTitle,
        audit: audit,
        report: axs.Audit.createReport(results)
      };
    });

    if (!ret) {
      console.error('Audit failed: Retries: ' + retry.count);
      phantom.exit(1);
      return;
    }

    console.log(JSON.stringify(ret));

    // Must do crazy song and dance to get phantom to exit properly
    // https://github.com/ariya/phantomjs/issues/12697
    webpage.close();
    setTimeout(function () {
      phantom.exit();
    }, 0);
  }, opts.delay * 1000);

  retry.count = retries++;
}

webpage.onResourceTimeout = function (err) {
  console.error('Error code:' + err.errorCode + ' ' + err.errorString + ' for ' + err.url);
  if (retry.count === RETRY_COUNT) {
    webpage.close();
    setTimeout(function () {
      phantom.exit(1);
    }, 0);
  } else {
    capture(retry.count);
    console.error('Retrying... ' + retry.count);
  }
};

webpage.onError = opts.verbose ? function (err, trace) {
  console.error('\n' + err + '\n' + formatTrace(trace[0]) + '\n');
} : function () {
};

webpage.open(opts.url, function () {
  if (status === 'fail') {
    console.error('Couldn\'t load url: ' + JSON.stringify(opts.url));
    phantom.exit(1);
  }

  // Inject axs_testing
  webpage.injectJs(TOOLS_PATH);

  // Begin the capture
  capture(RETRY_COUNT);
});
